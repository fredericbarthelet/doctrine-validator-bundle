<?php

namespace AssoConnect\DoctrineValidatorBundle\Doctrine\DBAL\Types;


class PhoneLandlineType extends PhoneType
{
    const TYPE = 'phonelandline';
}
