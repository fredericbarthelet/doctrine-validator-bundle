<?php

namespace AssoConnect\DoctrineValidatorBundle\Doctrine\DBAL\Types;

class PhoneMobileType extends PhoneType
{
    const TYPE = 'phonemobile';
}
