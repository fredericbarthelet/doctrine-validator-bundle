<?php

namespace AssoConnect\DoctrineValidatorBundle;

use AssoConnect\DoctrineValidatorBundle\DependencyInjection\AssoConnectDoctrineValidatorExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AssoConnectDoctrineValidatorBundle extends Bundle
{
}
